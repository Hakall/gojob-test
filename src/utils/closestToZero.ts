const reducer = (accumulator: number, currentValue: number): number => {
    return Math.abs(accumulator) < Math.abs(currentValue)
            ? accumulator
            : Math.abs(accumulator) !== Math.abs(currentValue)
            ? currentValue
            : accumulator < currentValue
                ? currentValue
                : accumulator;
};

export default (arr: number[] | undefined): number => {
    if(!Array.isArray(arr) || !arr.length) {
        return 0;
    }
    return arr.reduce(reducer);
}